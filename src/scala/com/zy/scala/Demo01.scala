package com.zy.scala


import scala.collection.mutable
import scala.collection.mutable.ListBuffer
import scala.util.Random

/**
  * 这个object主要是复习scala的基础语法
  */
object Demo01 {
  //主方法入口
  def main(args: Array[String]): Unit = {
    //1.数据类型(val 是不可改变，var 是可变)
    val int = 100
    val str="string"
    val byte='a'
    val char='a'
    val long=10L
    val float=10.1f
    val double=10.1
    val boolean=true

    //2. 控制流程语句
    val index=10
    if(index>3){
      println("index:"+index)
    }else{
      println(3)
    }
    //返回结果
    val y={
      if(index>3){
        index
      }else if (index>10){
        10
      }else{
        "error"
      }
    }

    //3.块表达式
    val value=if(true){
      "ok"
    }else{
      "error"
    }

    //4.循环
    val arr1=Array("1","2","3")
    for(ele<-arr1){
      println(ele)
    }
    val ind=for(i<- 1 to 10) yield i*10
    println(ind)

    // 迭代翻转
    val arr2 = Array(1, 2, 3, 4, 5, 6, 7, 8)
    for(i <- (0 until arr2.length).reverse ){ //until 是前闭后开的一个关键字
      println(arr2(i))
    }
    //6.定义函数
    val sum=(x:Int,y:Int)=>x+y
    println(sum(1,2))

    val arr = Array(1, 2, 3, 4, 5, 6, 7, 8, 9, 10)
    val res1=arr.map((x:Int)=>x*10)
    val res2=arr.map(x=>x*5) //这里因为Array已经有了数据类型的限制，这里可以不写出
    val res3=arr.map(_*10) //_ 表示arr中的，，每一个元素
    //完整版的函数定义
    val fun:(Int,Double)=>(Double,Int)={(x,y) =>(y,x)}
    val r=fun(1,2)

    //函数与方法相互转换
    def meth1(f1:(Int,Int)=>Int,x:Int,y:Int):Int={
      f1(x,y)
    }
    val f=(x:Int,y:Int)=>x+y

    def meth2(x:Int,y:Int):Int={
      return x+y
    }

    // 把函数传递到方法里面
    meth1(f,1,2)
    // 把方法传入到方法里面
    meth1(meth2 _,1,2)

    collectionTotal()


    var map1 = Map("xiaozhang" -> 2, "xiaowang" -> 4)
    var values = map1.get("a") match {
      case Some(i)=>{ //这里的i 就是 value
        i
      }
      case None=>{
        "没有找到"
      }
    }
    println(values)
  }

  //5.定义方法
  def function1(): Unit ={
    println("无参，无返回值")
  }
  def function2():String={
    "无参有返回值"
  }
  def function3(a:Int,b:Int):Int={
    val sum=a+b
    println("有参有返回值")
    return sum
  }

  //7. Array
  def testArr()={
    val arr1=Array(1,2,3,4)
    val arr2=Array("apache","beeline","c++")
    val arr3=Array(1,2,"hadoop","hive")

    //泛型
    import scala.collection.mutable.ArrayBuffer
    val ab=ArrayBuffer[Int](6)

    //遍历与过滤
    val arrq=Array(1, 2, 3, 4, 5, 6, 7, 8, 9)
    val res1=for(ele<-arrq if ele%2==0)yield ele*10
    val res2=arrq.filter(_%2==0).map(_*10)

    // 求和
    println(arr1.sum)
    // 查找最大值
    println(arr1.max)
    // 查找最小值
    println(arr1.min)
    // 对数据进行反转
    println(arr1.reverse)

    // 对数据进行正序排序
    println(arr1.sorted)

    // 对数据进行倒叙排序
    println(arr1.sortWith((x,y)=>x>y))

    // 对数据进行累加
    println(arr1.reduce((x:Int,y:Int)=>{x+y}))
    println(arr1.reduce(_+_))
  }

  //7. Map
  def testMap()={
    val map1=Map("a"->1,"b"->2)
    val map2=Map(("a",1),("b",2),("c",3))

    var maybeInt: Option[Int] = map1.get("a")
    var value: Any = map1.getOrElse("a", "defalut")
    println(map1.size)
    // 查看是否是实体
    println(map1.empty)
  }

  //8.List
  def testList()={
    val li1=List(1,2,3,4,5,6,7)
    // 在集合的头部追加元素
    val li2=8::li1
    // 往尾部追加数据
    val li3=li1.::(9)
    //转换为List
    var toList: List[Int] = li1.map(_ * 10).toList

    //初始化 list 集合
    val list = ListBuffer(1, 2, 2, 3, 3, 4, 545, 6, 4556)
    // 对第一个元素进行重新赋值
    list(0)=100
    // 在 list 的后面追加一个元素
    list+=888
    // 以元组的形式增加数据
    list+=(888,999,1000)
    // 在元素的尾部追加数据
    list.append(123456)
    var buffer: mutable.Buffer[Int] = list.toBuffer
    var newList: List[Int] = buffer.toList
  }

  //  9.set
  def testSet()={
    // 创建可变的 set 的集合
      val set =new mutable.HashSet[Int]()
    // 为元素赋值
    set+=1
    set++Set(1,2,3,4,5)
    set.add(123)
    set.remove(2)
  }
  def testHashMap()={
    val map = new mutable.HashMap[String,Int]()
    //增加元素
    map.put("zs",18)
    map("xiaowang") = 2
    map+=(("ww",18))
  }
  def collectionTotal()={
    //wordCount
    val lines=Array("hellow hell", "xiaozhang xiaowang", "xiao zhang",
      "da xiao budabuxiao", "xiao xiao xiao xiao");
    var wordCount: List[(String, Int)] = lines.flatMap(_.split("\\s+")).map((_, 1))
      .groupBy(_._1).map(t => (t._1, t._2.size))
      .toList.sortBy(_._2).toList
    print(wordCount)
  }

  //10. tuple
  def testTuple()={
    val value,(s,y,z)=(2,3,5)
    println(value._1) //2

    val index=(1,2,3)
    println(index._1)

    val t = (1, 3.14, "Fred")
    t.productIterator.foreach{
      i=> println("Value = " + i)
    }
    val t1 = new Tuple2("www.google.com", "www.runoob.com")
    //翻转
    println("反转后的元组: " + t1.swap)
  }


  //三个样例类
  case class Dome3(name: String, higth: String)
  case class Dome4(x: Int, y: Int)
  case class Dome5(z: Int)
  //11.模式匹配
  def testPatternMatching()={
    //.1匹配array
    val arr = Array(Dome3("333", "43"), Dome4(3, 5), Dome5(5))
    arr(Random.nextInt(arr.length)) match {
      case Dome3(name,height)=>{
        println("dome3")
      }
      case Dome4(3,5)=>{
        println("Dome4")
      }
      case Dome5(5)=>{
        println("exit")
      }
    }

    //.2匹配map
    val map = Map("a" -> 1, "b" -> 2, "c" -> 3)
    val v=map.get("a") match {
      case Some(i) => println("-------------------")
      case None=>println("++++++++++++++++++++++++++")
    }
    //.3方法
    def func(num:String):Int=num match {
      case "one" =>1
      case "two"=>2
      case _=> -1
    }
    //.4偏函数
    def func1:PartialFunction[String, Int]={
      case "one"=>{
        println("one case")
        1
      }
      case "two"=>2
      case _ => -1
    }



    //2.类型匹配
    val arr1 = Array("校长", "小王", "小李", "大和大", "回复", 3, 343.6, 'd',
      true)
    val name = arr1(Random.nextInt(arr1.length))
    name match{
      case "校长" => println("")
      case x:Int if(x>10) =>{
        println("Int:" + x)
      }
      case y:Double =>{
        println("Double:" + y)
      }
      case (_,_,"小李") =>{
        println("小李" )
      }
      case _ =>{
        println("exit")
      }
    }

    //3. 集合匹配
    val arr3 = Array(1, 1, 7, 0)
    arr3 match {
      case Array(1,1,x,y)=>{
        println(x + ":" + y)
      }
      case Array(0,_*)=>{
        println("0 ...")
      }
      case _ =>{
        println("something else")
      }
    }

    val lst = List(0)
    lst match{
      case 0::Nil => println("only 0")
      case x::y::Nil => println(s"x ${x} y ${y}")
      case 0::a=>println(s"0 ... $a")
      case _ =>println("something else")
    }

    //4. option 匹配

    var map1 = Map("xiaozhang" -> 2, "xiaowang" -> 4)
    var value = map1.get("xiaoming") match {
      case Some(i)=>{
        i
      }
      case None=>{
        "没有找到"
      }
    }
    //****等价于
    map1.getOrElse("xiaoming","没有找到")
  }

  //偏函数： 被花括号括起来的但是没有 match 的一组叫做偏函数
  def func11:PartialFunction[String, Int] ={
    case "one" => 1
    case "two" => 2
    case _ => -1
  }
}
