package com.zy.scala

import java.util.Date

/**
  * 这个demo主要说的是：类、对象、继承、超类
  */
object Demo02 {
  def main(args: Array[String]): Unit = {
    val per=Persion //Persion的伴生对象就是一个单例
    per.printPersion()
    val book=Book(1001,"斗破苍穹","李虎") //调用伴生对象的apply方法
    val eagle=new Eagle()
    eagle.shut()
    eagle.fly()
  }
}
class Persion{
  // 用 val 修饰的变量是只读属性
  val id=1010123
  var age:Int=18

  // 类私有字段,只能在类的内部使用
  private var name:String="zs"

  //伴生对象都访问不到
  private[this] var address="beijing"

}
//Persion 类的伴生对象
object Persion{
  def printPersion()={
    var persion=new Persion()
    println(persion.id)
    println(persion.age)
    println(persion.name)
  }
}

class Student(var name:String,var age:Int,faceValue: Double = 99.9){//主构造器
  private[this] var gender: String = null
  def show(): Unit ={
    println(faceValue)
  }
  //辅助构造器 def this （参数）
  def this(name:String,age:Int,gender:String){
    //辅助构造器第一行一定要先调用主构造器
    this(name,age)
    this.gender=gender
  }
  //应用程序对象
}

object Dau extends App{ //可以直接执行，运行 main 方法的另外一种方式
  println("sf")
}

//apply 方法的使用
class Book(var name:String,var author:String,var date:Date=new Date()){
  private[this] var id:Int=0
  def this(id:Int,name:String,author:String){
    this(name,author)
    this.id=id
  }
}
object Book{
  def apply(id:Int,name:String,author:String)={
    val book = new Book(id,name,author)
    show(book)
  }
  def show(book:Book)={
    println("name："+book.name)
    println("author："+book.author)
    println("date："+book.date)
  }
}

//继承 单继承，多实现
class Animal(var name:String,var age:Int){
  def shut()={
    println("嘤嘤嘤")
  }
}

class Dog extends Animal("xiaobxiao",2){
  override def shut(): Unit = {
    println("汪汪汪！")
  }
}
trait Fly{
  def fly()={
    println("翱翔蓝天！")
  }
}
class Eagle extends Animal("啾啾",2) with Fly{
  override def shut(): Unit = {
    println("鹰鸣！")
  }

  override def fly(): Unit = {
    println("鹏程万里！")
  }
}
