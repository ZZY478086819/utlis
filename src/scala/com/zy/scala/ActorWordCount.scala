package com.zy.scala

import scala.actors.{Actor, Future}
import scala.collection.immutable.HashSet
import scala.collection.mutable.ListBuffer
import scala.io.Source


case class SubmitTask(filePath: String)
case class ResultTask(result: Map[String, Int])
case object StopTask

class Task extends Actor{

  override def act(): Unit = {
    loop{
      react{
        case SubmitTask(filePath) =>{
          var result: Map[String, Int] = Source.fromFile(filePath).getLines()
            .toList.flatMap(_.split("\\s+")).map((_, 1))
            .groupBy(_._1).mapValues(_.size)
          sender ! ResultTask(result)
        }
        case StopTask =>{
          exit()
        }
      }
    }
  }
}

object ActorWordCount {
  def main(args: Array[String]): Unit = {
    var replySet =new HashSet[Future[Any]]()
    val resultList=new ListBuffer[ResultTask]()

    val list = Array[String]("D:\\workspace\\Utlis\\src\\scala\\files\\text1.txt", "D:\\workspace\\Utlis\\src\\scala\\files\\text2.txt","D:\\workspace\\Utlis\\src\\scala\\files\\text3.txt")
    for(filePath <-list){
      val task=new Task
      val reply =task.start !! SubmitTask(filePath)
      replySet+=reply

    }
    while(replySet.size>0){
      // _.isSet 判断是不是 Set 集合
      val toCompute=replySet.filter(_.isSet)
      for(f <-toCompute){
        // 把执行的结果付给 ResultTask
        val result =f().asInstanceOf[ResultTask]
        resultList+=result
        replySet-=f
      }
      Thread.sleep(1000)
    }
    var tuples: ListBuffer[(String, Int)] = resultList.flatMap(_.result)
    var stringToTuples: Map[String, ListBuffer[(String, Int)]] = tuples.groupBy(_._1)
    var stringToInt: Map[String, Int] = stringToTuples.mapValues(_.foldLeft(0)(_ + _._2))
    println(stringToInt)
  }
}


