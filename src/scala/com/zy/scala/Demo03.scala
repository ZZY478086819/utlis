package com.zy.scala

/**
  * 这里都是关于scala的高级操作(隐式转化、柯里化、泛型)
  */
import java.io.File

import scala.io.Source
object Demo03 {

  def buySpecialTicket(sp : SpecialPerson){
    println("buySpecialTicket success " + sp.name)
  }

  //1. 隐式转换
  implicit def file2RichFile(file:File):RichFile = new RichFile(file)
  class RichFile(file:File){
    def read() :String= {
      Source.fromFile(file).mkString
    }
  }
  //隐式转换做参数类型转换
  class SpecialPerson(val name : String)
  class Student(val name : String)
  class Older(val name : String)

  implicit def object2SpecialPerson(obj:Object):SpecialPerson ={
    obj match{
      case a:Student =>{
        val stu=obj.asInstanceOf[Student];new SpecialPerson(stu.name)
      }
      case b:Older =>{
        val old=obj.asInstanceOf[Older];new SpecialPerson(old.name)
      }
      case _ => new SpecialPerson(obj.toString)
    }
  }

  //2. 柯里化
  //初始化隐式参数
  object MyPreDef{
    implicit val aaaa: Int = 1024
  }
  // 定义一个普通方法
  def curry(x:Int,y:Int) ={
    x*y
  }
  // 定义柯里化方法一
  def currying(x:Int)(y:Int)={
    x*y
  }
  // 初始化隐实值
  def curryingImp(x:Int)(implicit y:Int =100)={
    x*y
  }

  // 定义柯里化方法二
  def curryingTest(x:Int) = (y:Int) =>{
    x*y
  }


  def main(args: Array[String]): Unit = {
    val file1=new File("D:\\workspace\\Utlis\\src\\scala\\files\\text1.txt");
    //装饰模式显示的增强
    val rf=new RichFile(file1);
    var read: String = rf.read()
    println(read)

    //隐式增强的方式
    val file=new File("D:\\workspace\\Utlis\\src\\scala\\files\\text1.txt");
    file.read() //隐式转换增强现有类型
    buySpecialTicket(new Student("学生"))
    buySpecialTicket(new Older("老人"))


    //调用柯里化方法
    var result1: Int = currying(3)(4)
    println(result1)
    // 把柯里化的方法转换为函数
    var intToInt: Int => Int = currying(3) _
    var result2:Int=intToInt(4)
    println(result2)

    // 带有隐士参数的柯里化方法
    var result3: Int = curryingImp(2)
    println(result3)

    import MyPreDef.aaaa //去 MyPreDef 查找类型匹配的参数，使用最新的参数，如果找不到则使用默认的
    val result4: Int = curryingImp(2)
    println(result4)

    // 测试柯里化方法二
    var test: Int => Int = curryingTest(2)
    val result5 = test(3)
    println(result5)


    //泛型之上界
    val mr = new MrRight[Boy]
    val boy1 = new Boy("xiaozhang", 88)
    val boy2 = new Boy("xiaowang", 8888)
    var choose: Boy = mr.choose(boy1, boy2)
    println(s"choose = ${choose.name}")


    //泛型之视图边界
    import MyPreDef1.girl2Ordered
    val mr2 = new MissRight[Girl]
    // 实例化对象信息
    val g1 = new Girl("xiaolili", 88)
    val g2 = new Girl("xiaomoli", 99)
    // 调用方法调用
    var girl: Girl = mr2.choose(g1, g2)
    println(s"choose = ${girl.name}")
    //上下文界定
    testOrdering_Object_Context_2()
  }
  //上下文界定
  def testOrdering_Object_Context_2(): Unit ={

    class Emp(val name: String, val faceValue: Int){}
    //定义隐式方法
    object MyPreDef {
      // 上下文转换的值
      implicit  object emp2Ordering extends  Ordering[Emp]{
        override def compare(x: Emp, y: Emp): Int = {
          x.faceValue-y.faceValue
        }
      }
    }
    // 上下文的界定不需要 Ordering 不需要指定泛型
    class MissRight[T:Ordering]{
      def select(first: T, second: T):T ={
        val ord = implicitly[Ordering[T]]
        if(ord.gt(first,second)) first else second
      }
    }
    import MyPreDef.emp2Ordering
    //上下文界定
    val mr3 = new MissRight[Emp]
    // 实例化对象信息
    val e1 = new Emp("xiaolili", 88)
    val e2 = new Emp("xiaomoli", 99)
    // 调用方法调用
    var emp: Emp = mr3.select(e1, e2)
    println(s"emp ： ${emp.name}")
  }
}
//3. 泛型
/**
  * [ T < : UpperBound ] 上界
  * [ T >: LowerBound] 下界
  * [ T <% ViewBound] 视图界定
  * [ T : ContextBound] 上下界
  * [ T + ] 斜变
  * [ -T ] 逆变
  *
  */
// 上界实例
class Boy(val name: String, val faceValue: Int) extends Comparable[Boy]{
  override def compareTo(o: Boy): Int = {
    this.faceValue-o.faceValue
  }
}
// 在类中定义泛型
class MrRight[T<:Comparable[T]]{
  def choose(first: T, second: T): T ={
    if(first.compareTo(second)>0){
        first
    }else{
      second
    }
  }
}

//视图界定实例
//定义实体类
class Girl(val name:String,val faceValue:Int)

//定义隐式方法
object MyPreDef1{
  // 隐式的方法
  implicit def girl2Ordered(g:Girl):Ordered[Girl] = new Ordered[Girl]{
    override def compare(that: Girl): Int = {
      g.faceValue-that.faceValue
    }
  }
}
class MissRight[T <% Ordered[T]]{
  def choose(first: T, second: T): T ={
    if(first>second){
        first
    }else{
      second
    }
  }
}

