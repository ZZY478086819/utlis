package redis;

import redis.clients.jedis.Jedis;

import java.util.Map;

/**
 * 通过使用Redis的hash实现BuyCart
 */
public class RedisHashDemo {
    private static Jedis jedis=new Jedis("192.168.130.66",6379);

    private static final String CART  = "cart";

    //添加商品
    public void addProductCart(){
        jedis.hset(CART+":username003","气球","100");
        jedis.hset(CART+":username004","饮料","20");
        jedis.hset(CART+":username005","泡面","30");
        jedis.hset(CART+":username006","火腿","40");
        jedis.hset(CART+":username007","饼干","35");

        jedis.close();
    }

    //查询购物车
    public void getProductInfo(){
        Map<String, String> pFor005  = jedis.hgetAll(CART + "username003");
        pFor005.forEach((k,v)-> System.out.println("key："+k+",value:"+v));
        jedis.close();
    }

    //修改购物车信息
    public void editProductInfo(){
        jedis.hset(CART+"username003","杜蕾斯","80");
        jedis.hincrBy(CART+"username003","杜蕾斯",10);

        jedis.close();
    }

    public static void main(String[] args) {
        RedisHashDemo redisHashDemo = new RedisHashDemo();
        System.out.println("-----------------set---------------------");
        redisHashDemo.addProductCart();
        System.out.println("-----------------edit---------------------");
        redisHashDemo.editProductInfo();
        System.out.println("-----------------getAll---------------------");
        redisHashDemo.getProductInfo();
    }
}
