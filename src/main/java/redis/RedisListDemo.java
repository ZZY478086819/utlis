package redis;

import redis.clients.jedis.Jedis;

import java.util.Random;
import java.util.UUID;

/**
 * List 类型 模拟生产任务与消费任务的形式展示:
 */
public class RedisListDemo {
    private static Jedis jedis = new Jedis("192.168.130.66", 6379);

    public static void main(String[] args) {
        TaskProduct  product=new TaskProduct();
        new Thread(product,"product").start();



        TaskConsumer  consumer=new TaskConsumer();
        new Thread(consumer,"consumer").start();


    }
    static private class TaskProduct implements Runnable {
        Random random = new Random();
        @Override
        public void run() {
            int num = 0;
            synchronized (TaskProduct.class) {
                while (num < 100) {
                    int nextInt = random.nextInt(100);
                    try {
                        Thread.sleep(100 + nextInt);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //模拟生产一个任务
                    String taskId = UUID.randomUUID().toString();
                    jedis.lpush("task-queue", taskId);
                    System.out.println("生产一个任务: " + taskId);
                    num++;
                }
            }
        }
    }

    static private class TaskConsumer  implements Runnable{
        Random random = new Random();
        @Override
        public void run() {
            int num = 0;
            synchronized (TaskProduct.class){
                int nextInt = random.nextInt(100);
                while(num<100){
                    try {
                        Thread.sleep(100 + nextInt);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    //从task-queue任务队列里取出一个任务的同时放入暂存队列中
                    String taskId = jedis.rpoplpush("task-queue","temp-queue");
                    //处理任务
                    if(random.nextInt(20)%9 == 0) {   //模拟任务失败的情况
                        //如果任务处理失败,把任务从暂存队列里弹出来放到任务队列里
                        jedis.rpoplpush("temp-queue","task-queue");
                        System.out.println("任务处理失败:"+taskId);
                    }else{
                        //任务处理成功
                        //如果任务处理失败,把暂存暂存队列里的任务删除
                        jedis.rpop("temp-queue");
                        System.out.println("任务处理成功:"+taskId);
                    }
                    num++;
                }
            }
        }
    }
}


