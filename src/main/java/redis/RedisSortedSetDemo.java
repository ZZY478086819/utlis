package redis;


import redis.clients.jedis.Jedis;
import redis.clients.jedis.Tuple;

import java.util.Random;
import java.util.Set;

/**
 * zset在设置时,会给设置一个分数,通过分数,可以进行排序
 * 模拟LOL任务出场:
 *
 */
public class RedisSortedSetDemo {
    private static Jedis jedis=new Jedis("192.168.130.66",6379);
    public void setSorted() throws InterruptedException{
        Random random = new Random();
        String[] heros = {"提莫","蒙多","木木","EZ","轮子妈","艾希","贾克斯","亚索","金克斯","奥巴马"};
        int num=0;
        while(true){
            num++;
            int index=random.nextInt(heros.length);
            String hero=heros[index];
            Thread.sleep(2000);
            jedis.zincrby("hero:cc1",1,hero);
            System.out.println(hero + "已经出场!");
            if(num>100){
                break;
            }
        }
    }
    //模拟LOL榜单显示
    public void lolBoxViewer() throws InterruptedException {
        int i =1;
        while(true){
            Thread.sleep(3000);
            System.out.println("--------------------------第"+i+"次查看榜单---------------------");
            //从Redis中查询榜单的前3名
            Set<Tuple> heros =jedis.zrevrangeWithScores("hero:cc1",0,2);
            for (Tuple hero: heros){
                System.out.println(hero.getElement()+"==="+hero.getScore());
            }
            i ++;
            System.out.println("");
        }
    }
    public static void main(String[] args) {

    }
}
