package redis;

import com.google.gson.Gson;
import redis.clients.jedis.Jedis;
import redis.pojo.ProductInfo;

import java.io.*;

public class RedisStringDemo {
    private static Jedis jedis=new Jedis("192.168.130.66",6379);

    //将字符串缓存到String数据结构中
    public void stringTest(){
        jedis.set("user:001:name2","dance");
        jedis.set("user:002:name2","G-Dragon");
        jedis.set("user:003:name2","xiaoming");
        jedis.set("user:004:name2","xiaofang");

        String username001 = jedis.get("user:001:name2");
        String username002 = jedis.get("user:002:name2");
        String username003 = jedis.get("user:003:name2");
        String username004 = jedis.get("user:004:name2");

        System.out.println(username001);
        System.out.println(username002);
        System.out.println(username003);
        System.out.println(username004);
    }

    //将对象缓存到String数据结构中
    public void objectTest() throws IOException, ClassNotFoundException {
         ProductInfo pro = new ProductInfo("热水袋","暖手","36");

        //将对象序列化成byte数组
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        ObjectOutputStream oos = new ObjectOutputStream(bos);

        //用对象序列化的方式将pro实例序列化并写入流中
        oos.writeObject(pro);

        //将bos转换成字节数组
        byte[] pByte =bos.toByteArray();

        //将序列化后的对象缓存到string数据结构中
        jedis.set("product:001".getBytes(), pByte);

        //获取数据
        byte[] pByteRes = jedis.get("product:001".getBytes());

        //将获取的数据反序列化
        ByteArrayInputStream bis = new ByteArrayInputStream(pByteRes);
        ObjectInputStream ois = new ObjectInputStream(bis);

        ProductInfo pRes =(ProductInfo)ois.readObject();

        System.out.println(pRes.toString());
    }

    //将对象转换成json格式的字符串缓存到String数据结构中
    public void objectToJsonTest(){
        ProductInfo pro = new ProductInfo();
        pro.setName("蜡烛");
        pro.setDesc("星星之火");
        pro.setPrice("8.88");

        //把对象转换成json格式
        Gson gson=new Gson();
        String json  = gson.toJson(pro);

        //存
        jedis.set("product:003",json);


        //取
        String jsonRes = jedis.get("product:003");

        //将json字符串转换成对象
        ProductInfo productInfo=gson.fromJson(jsonRes,ProductInfo.class);

        System.out.println(jsonRes);
        System.out.println(productInfo.toString());
    }

    public static void main(String[] args) throws IOException, ClassNotFoundException {
         RedisStringDemo redisStringDemo = new RedisStringDemo();
        System.out.println("--------------------string------------------------");
         //将字符串缓存到String数据结构中
        redisStringDemo.stringTest();
        System.out.println("--------------------obj------------------------");
        //将对象缓存到String数据结构中
        redisStringDemo.objectTest();
        System.out.println("--------------------json------------------------");
        //将对象转换成json格式的字符串缓存到String数据结构中
        redisStringDemo.objectToJsonTest();
    }
}
