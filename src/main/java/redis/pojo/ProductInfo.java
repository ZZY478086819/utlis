package redis.pojo;

import java.io.Serializable;

public class ProductInfo  implements Serializable {

    private static final long serialVersionUID = 3529816800985284998L;

    private String name;
    private String desc;
    private String price;

    public ProductInfo(){}
    public ProductInfo(String name, String desc, String price) {
        this.name = name;
        this.desc = desc;
        this.price = price;
    }

    @Override
    public String toString() {
        return "ProductInfo{" +
                "name='" + name + '\'' +
                ", desc='" + desc + '\'' +
                ", price='" + price + '\'' +
                '}';
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
