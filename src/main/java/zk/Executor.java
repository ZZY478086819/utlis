package zk;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;

import java.io.*;

/**
 * 这个案例是由官网给出的:
 *  A Simple Watch Client  其实代码一点不Simple
 *  ZooKeeper客户端监视ZooKeeper节点的更改，并通过启动或停止程序来响应。（程序 这里是自定义的可执行文件）
 *  实现内容：
 *      ☆它获取与znode关联的数据并启动可执行文件。
 *      ☆如果znode发生更改，客户端将重新获取内容并重新启动可执行文件。
 *      ☆如果znode消失，客户端将杀死可执行文件。
 */
public class Executor implements Watcher, Runnable, DataMonitor.DataMonitorListener {
    /**
     * Executor的类维护ZooKeeper连接
     * Executor包含主线程和执行逻辑，它负责很少的用户交互，
     * 以及与作为参数传入的可执行程序的交互，根据znode的状态，示例(根据需求)关闭并重新启动该程序。
     */
    String znode;
    DataMonitor dm;
    ZooKeeper zk;
    String filename;
    String exec[];
    Process child;

    public static void main(String[] args) {
        String hostPort = "2181";  //端口
        String znode = "/zy-test"; //znode名称
        String filename = "";      //
        String exec[] = {"", ""};
        try {
            new Executor(hostPort, znode, filename, exec).run();
        } catch (KeeperException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public Executor(String hostPort, String znode, String filename,
                    String exec[]) throws KeeperException, IOException {
        this.filename = filename;
        this.exec = exec;
        zk = new ZooKeeper(hostPort, 3000, this);
        dm = new DataMonitor(zk, znode, null, this);
    }

    @Override
    public void run() {
        synchronized (this) {
            while (!dm.dead) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    static class StreamWriter extends Thread {
        OutputStream os;
        InputStream is;
        StreamWriter(InputStream is, OutputStream os) {
            this.is = is;
            this.os = os;
            start();
        }
        @Override
        public void run(){
            byte b[] = new byte[80];
            int rc;
            try{
                while((rc=is.read(b))>0){
                    os.write(b, 0, rc);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void exists(byte[] data) {
        if(data==null){
            if(child !=null){
                System.out.println("Killing process");
                child.destroy();
                try {
                    child.waitFor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            child=null;
        }else{
            if(child !=null){
                System.out.println("Stopping child");
                child.destroy();
                try {
                    child.waitFor();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            //
            try {
                FileOutputStream fos = new FileOutputStream(filename);
                fos.write(data);
                fos.close();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            System.out.println("Starting child");
            try {
                child = Runtime.getRuntime().exec(exec);
                new StreamWriter(child.getInputStream(),System.out);
                new StreamWriter(child.getErrorStream(), System.err);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }



    @Override
    public void closing(int rc) {
        synchronized (this) {
            notifyAll();
        }
    }



    @Override
    public void process(WatchedEvent event) {
        dm.process(event);
    }
}
