package zk;

import org.apache.zookeeper.*;
import org.apache.zookeeper.data.Stat;

import java.io.IOException;

/**
 * 服务端代码实现:
 * ☆获取链接 ( process )
 * ☆利用 zookeeper 链接注册服务器的信息
 * ☆启动业务的逻辑实现
 * ☆服务器端不需要实现监听
 */
public class ZookeeperServer {
    private String groupNode = "sgroup";
    private String subNode = "sub";


    /**
     * 连接 zookeeper
     */
    public void connectZookeeper(String address) throws IOException, KeeperException, InterruptedException {
        ZooKeeper zk = new ZooKeeper("" +
                "hadoop01:2181,hadoop02:2181,hadoop03:2181",
                5000, new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                // 不做处理
            }
        });

        /*
            在"/sgroup"下创建子节点
            子节点的类型设置为 EPHEMERAL_SEQUENTIAL
         */
        Stat exists = zk.exists("/"+groupNode, null);
        if(exists==null){
            String  createPath=zk.create("/" + groupNode,"sgroup".getBytes("utf-8"),
                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                    CreateMode.PERSISTENT);
            System.out.println("create: " + createPath);
        }

        String createPath = zk.create("/" + groupNode+"/"+subNode,
                address.getBytes("utf-8"),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.EPHEMERAL_SEQUENTIAL);
        System.out.println("create: " + createPath);
    }
    /**
     * server 的工作逻辑写在这个方法中 此处不做任何处理, 只让 server sleep
     */
    public void handle() throws InterruptedException {
        Thread.sleep(Long.MAX_VALUE);
    }

    public static void main(String[] args) throws InterruptedException, IOException, KeeperException {
        ZookeeperServer as = new ZookeeperServer();
        as.connectZookeeper(args[0]);
        as.handle();
    }
}
