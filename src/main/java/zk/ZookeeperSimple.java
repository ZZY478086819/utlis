package zk;

import org.apache.zookeeper.*;

import java.io.IOException;

/**
 * zookeeper 的基本的操作
 */
public class ZookeeperSimple {
    // 会话超时时间，设置为与系统默认时间一致
    private static final int SESSION_TIMEOUT = 30000;
    //创建 ZooKeeper 实例
    ZooKeeper zk;
    //server
    private String connectString="hadoop01:2181";

    // 创建 Watcher 实例
    private  Watcher wh = new Watcher(){
        @Override
        public void process(WatchedEvent event) {
            System.out.println(event.toString());
        }
    };

    /**
     * 初始化 zookeeper 的操作
     */
    private void createZKInstance() throws IOException {
        zk=new ZooKeeper(this.connectString,ZookeeperSimple.SESSION_TIMEOUT,
                this.wh);
    }

    /**
     * * zookeeper 的基本的操作
     */
    private void ZKOperations() throws KeeperException, InterruptedException {
        /**
         * ZK 的节点有 5 种操作权限：
         * CREATE:增（CREATOR_ALL_ACL）
         * READ:查（READ_ACL_UNSAFE）
         * WRITE:改（）
         * DELETE:删
         * ADMIN:管理（ANYONE_ID_UNSAFE）
         *
         * OPEN_ACL_UNSAFE：完全开放的ACL
         *
         * ALL = READ | WRITE | CREATE | DELETE | ADMIN;
         *
         * zk的znode有四种类型：
         * PERSISTENT：永久无编号
         * PERSISTENT_SEQUENTIAL：永久有编号
         * EPHEMERAL：临时无编号
         * EPHEMERAL_SEQUENTIAL：临时有编号
         */
        // 创建节点
        String newZnode="/zy-test/znode_3";
        zk.create(newZnode,"znode_3".getBytes(),
                ZooDefs.Ids.OPEN_ACL_UNSAFE,
                CreateMode.PERSISTENT);

        //修改节点
        zk.setData("/zy-test/znode_3","change znode_3".getBytes(),
               -1 );

        //获取修改节点的状态
        System.out.println(new String(zk.getData("/zy-test/znode_3", this.wh,
                        null)));

        //删除节点
        zk.delete(newZnode,-1);

        // 查看删除节点的状态(不存在返回null)
        System.out.println("节点状态：" + zk.exists("/zy-test/znode_3",
                this.wh));
    }

    /**
     * 关闭 zookeeper 的链接
     */
    private void zkClose() throws InterruptedException {
        zk.close();
    }

    /**
     * 主程序入口
     * @param args
     */
    public static void main(String[] args) throws InterruptedException, KeeperException, IOException {
        //init
        ZookeeperSimple dm = new ZookeeperSimple();
        dm.createZKInstance();
        //option
        dm.ZKOperations();
        //close
        dm.zkClose();
    }
}
