package zk;

import org.apache.zookeeper.*;

import java.io.IOException;
import java.util.Collections;
import java.util.List;
import java.util.Random;

/**
 * 分布式共享锁
 *  需求：在我们自己的分布式业务系统中，可能会存在某种资源，需要被整个系统的各台服务器
 * 共享访问，但是只允许一台服务器同时访问
 *
 * 实现原理：
 *  先查看所有机器上的锁，查看自己的锁是不是最小的，如果是最小的则会访问最大的
 * 那把锁，并把数据同步下来，访问完后把自己的锁删掉后，再会重新注册一把新锁
 */
public class DistributedClientLock {
    // 会话超时
    private static final int SESSION_TIMEOUT = 2000;
    // zookeeper 集群地址
    private String hosts = "hadoop01:2181,hadoop02:2181,hadoop03:2181";

    private String groupNode = "locks";

    private String subNode = "sub";

    private boolean haveLock = false;

    private ZooKeeper zk;

    private Watcher watcher;

    // 记录自己创建的子节点路径
    private volatile String thisPath;


    public void initWatcher(){
        watcher=new Watcher(){
            @Override
            public void process(WatchedEvent event) {
                // 判断事件类型，此处只处理子节点变化事件
                if(event.getType()==Event.EventType.NodeChildrenChanged&&
                        event.getPath().equals("/"+groupNode)){
                    // 获取子节点，并对父节点进行监听
                    try {
                        List<String> childrenNodes = zk.getChildren("/"+groupNode,true);
                        String thisNode=thisPath.substring(("/" + groupNode +
                                "/").length());
                        // 去比较是否自己是最小 id
                        Collections.sort(childrenNodes);
                        if(childrenNodes.indexOf(thisNode)==0&&haveLock){
                            // 访问共享资源处理业务，并且在处理完成之后删除锁
                            doSomething();
                            //重新注册一把新的锁
                            thisPath=zk.create("/"+thisPath+"/"+subNode,null,
                                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                                    CreateMode.EPHEMERAL_SEQUENTIAL);
                            haveLock=true;
                        }
                    } catch (KeeperException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        };
    }

    /**
     * 处理业务逻辑，并且在最后释放锁
     */
    private void doSomething() throws InterruptedException, KeeperException {
        try {
            System.out.println("gain lock: " + thisPath);
            Thread.sleep(2000);
            // do something
        }finally {
            System.out.println("finished: " + thisPath);
            // 访问完毕后，需要手动去删除之前的锁节点,-1 代表删除所有的版本的记录信息。
            System.out.println("释放锁，删除"+thisPath);
            zk.delete(this.thisPath,-1);
            haveLock=false;
        }
    }

    /**
     * 连接 zookeeper
     */

    public void connectZookeeper() throws IOException, KeeperException, InterruptedException {
        //初始化监听器
        initWatcher();
        zk=new ZooKeeper(hosts,
                SESSION_TIMEOUT,watcher);

        //1、程序一进来就先注册一把锁到 zk 上
        thisPath = zk.create("/" + groupNode + "/" + subNode, null,
                ZooDefs.Ids.OPEN_ACL_UNSAFE, CreateMode.EPHEMERAL_SEQUENTIAL);
        haveLock=true;
        // wait 一小会，便于观察
        Thread.sleep(new Random().nextInt(1000));

        // 从 zk 的锁父目录下，获取所有子节点，并且注册对父节点的监听
        List<String> childrenNodes = zk.getChildren("/" + groupNode,
                true);

        // 如果争抢资源的程序就只有自己，则可以直接去访问共享资源
        if(childrenNodes.size()==1&&haveLock){
            doSomething();
            thisPath = zk.create("/" + groupNode + "/" + subNode, null,
                    ZooDefs.Ids.OPEN_ACL_UNSAFE,
                    CreateMode.EPHEMERAL_SEQUENTIAL);
            haveLock=true;
        }
    }

    public static void main(String[] args) throws Exception {
        DistributedClientLock dl = new DistributedClientLock();
        dl.connectZookeeper();
        Thread.sleep(Long.MAX_VALUE);
    }
}
