package zk;

import org.apache.zookeeper.KeeperException;
import org.apache.zookeeper.WatchedEvent;
import org.apache.zookeeper.Watcher;
import org.apache.zookeeper.ZooKeeper;
import org.apache.zookeeper.data.Stat;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * 动态感知 Client
 */
public class ZookeeperClient {
    private String groupNode = "sgroup";
    private ZooKeeper zk;
    private Stat stat = new Stat();
    private volatile List<String> serverList;


    /**
     * 连接 zookeeper
     */
    public void connectZookeeper() throws Exception {
         zk = new ZooKeeper("" +
                "hadoop01:2181,hadoop02:2181,hadoop03:2181",
                5000, new Watcher() {
            @Override
            public void process(WatchedEvent event) {
                // 如果发生了"/sgroup"节点下的子节点变化事件, 更新
                //server 列表, 并重新注册监听
                if (event.getType() == Event.EventType.NodeChildrenChanged &&
                        ("/" + groupNode).equals(event.getPath())) {
                    try {
                        updateServerList();
                    } catch (KeeperException e) {
                        e.printStackTrace();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    } catch (UnsupportedEncodingException e) {
                        e.printStackTrace();
                    }
                }
            }
        });
        updateServerList();
    }

    /**
     * 更新 server 列表
     */
    private void updateServerList() throws KeeperException, InterruptedException, UnsupportedEncodingException {
        List<String> newServerList = new ArrayList<String>();
        // 获取并监听 groupNode 的子节点变化
        // watch 参数为 true, 表示监听子节点变化事件.
        // 每次都需要重新注册监听, 因为一次注册, 只能监听一次事件, 如果还想继续保持监听, 必须重新注册
        List<String> subList = zk.getChildren("/" + groupNode, true);
        for (String subNode : subList) {
            // 获取每个子节点下关联的 server 地址
            byte[] data = zk.getData("/" + groupNode + "/" + subNode, false, stat);
            newServerList.add(new String(data, "utf-8"));
        }
        // 替换 server 列表
        this.serverList = newServerList;
        System.out.println("server list updated: " + serverList.size());
    }


    /**
     * client 的工作逻辑写在这个方法中 此处不做任何处理, 只让 client sleep
     */
    public void handle() throws InterruptedException {
        Thread.sleep(Long.MAX_VALUE);
    }

    public static void main(String[] args) throws Exception {
        ZookeeperClient ac = new ZookeeperClient();
        ac.connectZookeeper();
        ac.handle();
    }

}
