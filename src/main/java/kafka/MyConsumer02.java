package kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/10
 *  * Time: 8:55
 *  * Description: 实现多分区消费
 */
public class MyConsumer02 {
    private static Properties props = new Properties();
    static{
        //设置kafka集群的地址
        props.put("bootstrap.servers", "hadoop01:9092,hadoop02:9092,hadoop03:9092");
        //设置消费者组，组名字自定义，组名字相同的消费者在一个组
        props.put("group.id", "kafka_api_group_1");
        //开启offset自动提交
        props.put("enable.auto.commit", "false");
        //序列化器
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    }

    public static void main(String[] args) {
        String topicName="kafka_api_r1p3";
        //实例化一个消费者
        KafkaConsumer<String,String> consumer =new KafkaConsumer<>(props);
        //消费者订阅主题，可以订阅多个主题
        consumer.subscribe(Arrays.asList(topicName));
        while(true){
            ConsumerRecords<String, String> records  = consumer.poll(Long.MAX_VALUE);
            //获取每个分区的数据
            for(TopicPartition partition :records.partitions()){
                System.out.println("开始消费第"+partition.partition()+"分区数据！");
                List<ConsumerRecord<String, String>> partitionRecords  = records.records(partition);
                //获取每个分区里的records
                for(ConsumerRecord<String, String> partitionRecord:partitionRecords){
                    System.out.println("partition:"+partition.partition()+",key:"+partitionRecord.key()+",value"
                    +partitionRecord.value()+",offset:"+partitionRecord.offset());
                }
                //更新每个分区的偏移量（取分区中最后一个record的偏移量，就是这个分区的偏移量）
                long lastOffset =partitionRecords.get(partitionRecords.size()-1).offset();
                consumer.commitSync(Collections.singletonMap(partition,new OffsetAndMetadata(lastOffset +1)));
            }
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
