package kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Properties;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/9
 *  * Time: 19:44
 *  * Description:  简单实现，kafka的消费者，并且将由kafka自动管理偏移量（单分区消费）
 */
public class MyConsumer01 {
    private static Properties props = new Properties();

    static {
        props.put("group.id", "kafka_api_group_2");
        //设置kafka集群的地址
        props.put("bootstrap.servers", "hadoop01:9092,hadoop02:9092,hadoop03:9092");
        //开启offset自动提交
        props.put("enable.auto.commit", "true");
        //手动提交偏移量
        //props.put("enable.auto.commit", "false");
        //设置自动提交时间
        props.put("auto.commit.interval.ms", "100");
        //设置消费方式
        props.put("auto.offset.reset","earliest");
        //序列化器
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
    }


    public static void main(String[] args) throws InterruptedException {
        String topic = "kafka_api_r1p1";
        //实例化一个消费者
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        //消费者订阅主题，可以订阅多个主题
//        consumer.subscribe(Collections.singleton(topic));
        consumer.subscribe(Arrays.asList(topic));
        //死循环不停的从broker中拿数据
        while(true){
            ConsumerRecords<String, String> records = consumer.poll(10);
            for(ConsumerRecord<String, String> record : records){
                System.out.printf("offset=%d,key=%s,value=%s",record.offset(),
                        record.key(),record.value());
            }
            Thread.sleep(2000);
        }
        //consumer.commitAsync(); 提交偏移量信息
    }
}
