package kafka;

import kafka.admin.AdminUtils;
import kafka.admin.RackAwareMode;
import kafka.utils.ZkUtils;
import kafka.utils.ZkUtils$;
import org.apache.kafka.common.security.JaasUtils;
import org.apache.zookeeper.ZKUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/9
 *  * Time: 18:59
 *  * Description:  主要做一些对kafkatopic创建、删除、查看；以及对组和分区的相关操作
 */
public class MyKafkaUtils {
    private static Properties props = new Properties();
    static {
        InputStream inputStream = MyProducer.class.getClassLoader().getResourceAsStream("utils.properties");
        try {
            props.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    //创建topic
    static public void createTopic(String topic,int partitions,int replications){
        String zkServers=props.getProperty("zkservers");
        ZkUtils zkUtils =ZkUtils.apply(zkServers,30000, 30000,JaasUtils.isZkSecurityEnabled());
        //创建topic
        AdminUtils.createTopic(zkUtils,topic,partitions,replications,new Properties(),
                RackAwareMode.Enforced$.MODULE$);
        zkUtils.close();
    }
   //查看topic是否存在
    static public boolean topicExists(String topic){
        String zkServers=props.getProperty("zkservers");
        ZkUtils zkUtils =ZkUtils.apply(zkServers,30000, 30000,JaasUtils.isZkSecurityEnabled());
        //创建topic
        boolean exists =AdminUtils.topicExists(zkUtils,topic);
        return exists;
    }
}
