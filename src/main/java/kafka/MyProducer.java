package kafka;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.zookeeper.ZKUtil;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/9
 *  * Time: 18:45
 *  * Description: 用于测试kafka的生产者 
 */
public class MyProducer {
    private String topic="";
    private static Properties props = new Properties();
    static {
        InputStream inputStream = MyProducer.class.getClassLoader().getResourceAsStream("kafka-args.properties");
        try {
            props.load(inputStream);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws ExecutionException, InterruptedException {
        KafkaProducer <String,String> producer=new KafkaProducer<>(props);
        String topic="kafka_api_r1p3";
        if(!MyKafkaUtils.topicExists(topic)){
            MyKafkaUtils.createTopic(topic,1,1);
        }
        for(int i=0;i<3000;i++){
            System.out.println("发送数据："+i);
            Future future =producer.send(new ProducerRecord<String,String>(topic,Integer.toString(i),
                    "zy:"+i));
            //打印发送数据是否成功
            System.out.println(future.get());
        }
        producer.close();
    }

}
