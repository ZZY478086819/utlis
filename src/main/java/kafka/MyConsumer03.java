package kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.consumer.OffsetAndMetadata;
import org.apache.kafka.common.TopicPartition;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Properties;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/10
 *  * Time: 10:10
 *  * Description: 消费者从指定分区拉取数据
 *      一旦指定特定的分区消费需要注意：
 *          （1）kafka提供的消费者组内的协调功能就不再有效
 *          （2）样的写法可能出现不同消费者分配了相同的分区，为了避免偏移量提交冲突，每个消费者实例的group_id要不重复
 */
public class MyConsumer03 {
    private static Properties props = new Properties();
    //实例化一个消费者
    static KafkaConsumer<String, String> consumer;
    static {
        //设置kafka集群的地址
        props.put("bootstrap.servers", "hadoop01:9092,hadoop02:9092,hadoop03:9092");
        //设置消费者组，组名字自定义，组名字相同的消费者在一个组
        props.put("group.id", "kafka_api_group_1");
        //开启offset自动提交
        props.put("enable.auto.commit", "false");
        //序列化器
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        consumer = new KafkaConsumer<>(props);
    }

    public static void main(String[] args) {
        //消费者订阅主题，并设置要拉取的分区
        String topic="kafka_api_r1p3";
        int partitionNum=0;
        //消费者订阅主题，并设置要拉取的分区
        TopicPartition partition0 =new TopicPartition(topic,partitionNum);
        consumer.assign(Arrays.asList(partition0));
        while(true){
            ConsumerRecords<String, String> records  = consumer.poll(Long.MAX_VALUE);
            for(TopicPartition partition : records.partitions()){
                List<ConsumerRecord<String, String>> partitionRecords  = records.records(partition);
                for(ConsumerRecord<String, String> partitionRecord:partitionRecords){
                    System.out.println("分区："+partitionRecord.partition()+",key："+partitionRecord.key()+",value："
                            +partitionRecord.value()+"offset："+partitionRecord.offset());
                }
                long lastOffset =partitionRecords.get(partitionRecords.size()-1).offset();
                consumer.commitSync(Collections.singletonMap(partition,new OffsetAndMetadata(lastOffset+1)));
            }
        }
    }
}
