package kafka;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.TopicPartition;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Properties;

/**
 *  * Created with IntelliJ IDEA.
 *  * User: ZZY
 *  * Date: 2019/9/10
 *  * Time: 9:46
 *  * Description:  该API用于重置kafka组的offset
 */
public class ReSetOffset {
    //用于重置的offset
    final private static String group="kafka_api_group_1";
    final private static Properties props = new Properties();
    static KafkaConsumer<String,String> consumer;
    static{
		props.put("bootstrap.servers", "hadoop01:9092,hadoop02:9092,hadoop03:9092");
		props.put("group.id",group);
		props.put("enable.auto.commit", "true");
    //props.put("auto.offset.reset","earliest");
		props.put("auto.commit.interval.ms", "1000");
		props.put("key.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
		props.put("value.deserializer",
                "org.apache.kafka.common.serialization.StringDeserializer");
        consumer=new KafkaConsumer<String, String>(props);
    }
    public static String resetOffset(String topic,long offset){
        int partitionNums=getTopicPartitionNum(topic);
        for(int i=0;i<partitionNums;i++){
            TopicPartition tp=new TopicPartition(topic,i);
            //这里每重置一个分区的offset，就需要重新创建一个新的KafkaConsumer
            KafkaConsumer consumer_temp= new KafkaConsumer<String, String>(props);
            consumer_temp.assign(Arrays.asList(tp));
            consumer_temp.seek(tp,offset);
            consumer_temp.close();
        }
        consumer.close();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss ");
        return dateFormat.format(new Date())+ group +" ResetOffset Succeed!!";
    }
    private  static int  getTopicPartitionNum(String topic){
        int partitionNums=consumer.partitionsFor(topic).size();
        return partitionNums;
    }

    public static void main(String[] args) {
        String topic="kafka_api_r1p1";
        System.out.println(ReSetOffset.resetOffset(topic,0));
    }
}
