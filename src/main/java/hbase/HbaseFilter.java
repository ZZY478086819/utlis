package hbase;


import org.apache.commons.io.filefilter.PrefixFileFilter;
import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.Cell;
import org.apache.hadoop.hbase.CellScanner;
import org.apache.hadoop.hbase.HBaseConfiguration;
import org.apache.hadoop.hbase.TableName;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.*;
import org.apache.hadoop.hbase.util.Bytes;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;
import java.util.Iterator;

/**
 * ：过滤器的类型很多，但是可以分为两大类 : 比较过滤器，专用过滤器
 *   (1) hbase 过滤器的比较运算符：
 *          LESS <
 *          LESS_OR_EQUAL <=
 *          EQUAL = NOT_EQUAL <>
 *          GREATER_OR_EQUAL >= GREATER >
 *          NO_OP 排除所有
 *   (2) Hbase 过滤器的比较器:
 *          BinaryComparator 按字节索引顺序比较指定字节数组，采用 Bytes.compareTo(byte[])
 *          BinaryPrefixComparator 跟前面相同，只是比较左端的数据是否相同
 *          NullComparator 判断给定的是否为空
 *          BitComparator 按位比较
 *          RegexStringComparator 提供一个正则的比较器，仅支持 EQUAL 和非 EQUAL
 *          SubstringComparator 判断提供的子串是否出现在 value 中。
 */
public class HbaseFilter {
    Configuration conf;
    Connection conn;
    private String zkCluster = "hadoop01:2181,hadoop02:2181,hadoop03:2181";


    @Before
    public void init() throws IOException {
        conf=HBaseConfiguration.create();
        // 对于 hbase 的客户端来说，只需要知道 hbase 所使用的 zookeeper 集群地址就可以了
        conf.set("hbase.zookeeper.quorum", zkCluster);
        conn=ConnectionFactory.createConnection(conf);
    }

    //@Test
    public void testFilter() throws IOException {
        String tableName = "emp_info";
        // 针对行键的前缀过滤器
        PrefixFilter pf1 = new PrefixFilter(Bytes.toBytes("liu"));
        testScann(pf1,tableName);

        // 行过滤器
        RowFilter rf1=new RowFilter(
                CompareFilter.CompareOp.LESS,
                new BinaryComparator(Bytes.toBytes("user002"))
        );
        RowFilter rf2=new RowFilter(
                CompareFilter.CompareOp.EQUAL,
                new SubstringComparator("00")
        );

        // 针对指定一个列的 value 来过滤(单列值过滤器:会返回满足条件的整行)
        SingleColumnValueFilter scvf1=new SingleColumnValueFilter("base_info".getBytes(),
                "password".getBytes(),
                CompareFilter.CompareOp.GREATER,
                "123456".getBytes());
        // 如果不设置为 true，则那些不包含指定 column 的行也会返回
        scvf1.setFilterIfMissing(true);

        ByteArrayComparable comparator1=new RegexStringComparator("^zhang");
        ByteArrayComparable comparator2 = new SubstringComparator("ang");
        SingleColumnValueFilter scvf2=new SingleColumnValueFilter("base_info".getBytes(),
                "username".getBytes(),
                CompareFilter.CompareOp.EQUAL,
                comparator1);

        // 针对列族名的过滤器 返回结果中只会包含满足条件的列族中的数据
        FamilyFilter ff1 =new FamilyFilter(CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes("inf")));
        FamilyFilter ff2 =new FamilyFilter(CompareFilter.CompareOp.EQUAL,
                new BinaryPrefixComparator(Bytes.toBytes("base")));


        //针对列名的过滤器 返回结果中只会包含满足条件的列的数据
        QualifierFilter qf1 =new QualifierFilter(CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes("password")));

        QualifierFilter qf2 = new QualifierFilter(CompareFilter.CompareOp.EQUAL,
                new BinaryPrefixComparator(Bytes.toBytes("us")));

        // 跟 SingleColumnValueFilter 结果不同，只返回符合条件的该 column
        ColumnPrefixFilter cf = new ColumnPrefixFilter("passw".getBytes());
        byte [][]prefixs=new byte[][]{
                Bytes.toBytes("username"),Bytes.toBytes("password")
        };
        MultipleColumnPrefixFilter mcf=new MultipleColumnPrefixFilter(prefixs);


        FilterList filterList =new FilterList(FilterList.Operator.MUST_PASS_ALL);
        filterList.addFilter(cf);
        filterList.addFilter(ff2);

    }
    public void testScann(Filter filter,String tableName) throws IOException {
        Table emp_info =
                conn.getTable(TableName.valueOf(tableName));
        Scan scan=new Scan();
        scan.setFilter(filter);
        ResultScanner scanner = emp_info.getScanner(scan);
        Iterator<Result> iter = scanner.iterator();
        while (iter.hasNext()) {
            Result result = iter.next();
            CellScanner cellScanner = result.cellScanner();
            while (cellScanner.advance()) {
                Cell current = cellScanner.current();
                byte[] familyArray = current.getFamilyArray();
                byte[] valueArray = current.getValueArray();
                byte[] qualifierArray = current.getQualifierArray();
                byte[] rowArray = current.getRowArray();
                System.out.println(new String(rowArray,
                        current.getRowOffset(),
                        current.getRowLength()));
                System.out.print(new String(familyArray, current
                        .getFamilyOffset(), current.getFamilyLength()));
                System.out.print(":"
                        + new String(qualifierArray, current
                        .getQualifierOffset(), current
                        .getQualifierLength()));
                System.out.println(" "
                        + new String(valueArray,
                        current.getValueOffset(),
                        current.getValueLength()));
            }
            System.out.println("-----------------------");
        }
    }

    /**
     * 多种过滤条件的使用方法
     */
    @Test
    public void kindOfFilter() throws IOException {
        Scan scan =new Scan(Bytes.toBytes("zhang_sh_01"),
                Bytes.toBytes("zhang_sh_02"));
        String tableName = "emp_info";
        //前缀过滤器----针对行键
        Filter filter = new PrefixFilter(Bytes.toBytes("zhang"));
        //testScann(filter,tableName);

        //单值过滤器 1 完整匹配字节数组
        SingleColumnValueFilter single1= new SingleColumnValueFilter("base_info".getBytes(),
                "name".getBytes(),CompareFilter.CompareOp.EQUAL,
                "lisi".getBytes());

        //testScann(single1,tableName);
        SubstringComparator comparator = new SubstringComparator("li");
         filter=new SingleColumnValueFilter("base_info".getBytes(),
                "NAME".getBytes(),CompareFilter.CompareOp.EQUAL,comparator);
        ((SingleColumnValueFilter) filter).setFilterIfMissing(true);
        //testScann(single1,tableName);

        //键值对元数据过滤-----family 过滤----字节数组完整匹配
        FamilyFilter ff = new FamilyFilter(CompareFilter.CompareOp.EQUAL,
                new BinaryComparator(Bytes.toBytes("base_info")));
       // testScann(ff,tableName);

        //键值对元数据过滤-----qualifier 过滤----字节数组完整匹配
        filter= new QualifierFilter(CompareFilter.CompareOp.EQUAL,
                new SubstringComparator("name"));
        //testScann(filter,tableName);

        //基于列名(即 Qualifier)多个前缀过滤数据的
        byte[][] prefixes = new byte[][] {Bytes.toBytes("user"),
                Bytes.toBytes("pas"),Bytes.toBytes("mar")};

        filter = new MultipleColumnPrefixFilter(prefixes);
        testScann(filter,tableName);
    }
}
