package hbase;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.hbase.*;
import org.apache.hadoop.hbase.client.*;
import org.apache.hadoop.hbase.filter.Filter;
import org.apache.hadoop.hbase.filter.PageFilter;
import org.apache.hadoop.hbase.mapreduce.HashTable;
import org.apache.hadoop.hbase.regionserver.BloomType;
import org.apache.hadoop.hbase.util.Bytes;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

public class HbaseDemo {
    private Configuration conf = null;
    private Connection conn = null;
    private String zkCluster = "hadoop01:2181,hadoop02:2181,hadoop03:2181";

    public HbaseDemo() {
        conf = HBaseConfiguration.create();
        // 对于 hbase 的客户端来说，只需要知道 hbase 所使用的 zookeeper 集群地址就可以了
        conf.set("hbase.zookeeper.quorum", zkCluster);
        try {
            conn = ConnectionFactory.createConnection(conf);
        } catch (IOException e) {
            System.out.println("连接 HBASE失败");
            e.printStackTrace();
        }
    }

    /**
     * 1. 建表
     */
    public void createHbaseTable(String tableName, String[] colFamilys) throws IOException {
        //获取一个表管理器
        Admin admin = conn.getAdmin();
        // 构造一个表描述器，并指定表名
        HTableDescriptor htd = new HTableDescriptor(TableName.valueOf(tableName));

        // 构造列族描述器，并指定列族名
        for (String cf : colFamilys) {
            HColumnDescriptor hColumnDescriptor = new HColumnDescriptor(cf);
            hColumnDescriptor.setBloomFilterType(BloomType.ROW).setVersions(1, 3);
            // 将列族描述器添加到表描述器中
            htd.addFamily(hColumnDescriptor);
        }
        admin.createTable(htd);
        admin.close();
        conn.close();
    }

    /**
     * 2.删除表
     */
    public void dropTable(String tableName) throws IOException {
        Admin admin = conn.getAdmin();
        admin.disableTable(TableName.valueOf(tableName));
        admin.deleteTable(TableName.valueOf(tableName));
        admin.close();
        conn.close();
    }

    /**
     * 3. 修改表定义（schema）
     */
    public void modifySchema(String tableName) throws IOException {
        Admin admin = conn.getAdmin();
        // 修改已有的 ColumnFamily
        HTableDescriptor table = admin.getTableDescriptor(TableName
                .valueOf(tableName));
        HColumnDescriptor family = table.getFamily("extra_info".getBytes());
        family.setBloomFilterType(BloomType.ROWCOL);
        table.addFamily(new HColumnDescriptor("other_info"));
        admin.modifyTable(TableName.valueOf(tableName), table);
        admin.close();
        conn.close();
    }

    /**
     * 4. 插入/修改 数据 DML
     */
    public void putData(String tableName) throws IOException {
        Table table = conn.getTable(TableName.valueOf(tableName));
        ArrayList<Put> puts = new ArrayList<Put>();
        // 构建一个 put 对象（kv），指定其行键
        Put put01 = new Put(Bytes.toBytes("emp001"));
        put01.addColumn(Bytes.toBytes("base_info")
                , Bytes.toBytes("username"),
                Bytes.toBytes("zs"));
        Put put02 = new Put("emp002".getBytes());
        put02.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("password"),
                Bytes.toBytes("123456"));
        Put put03 = new Put("user002".getBytes());
        put03.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("lisi"));
        put03.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));
        Put put04 = new Put("zhang_sh_01".getBytes());
        put04.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("zhang01"));
        put04.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));
        Put put05 = new Put("zhang_sh_02".getBytes());
        put05.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("zhang02"));
        put05.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));
        Put put06 = new Put("liu_sh_01".getBytes());
        put06.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("liu01"));
        put06.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));
        Put put07 = new Put("zhang_bj_01".getBytes());
        put07.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("zhang03"));
        put07.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));
        Put put08 = new Put("zhang_bj_01".getBytes());

        put08.addColumn(Bytes.toBytes("base_info"),
                Bytes.toBytes("username"),
                Bytes.toBytes("zhang04"));
        put08.addColumn(Bytes.toBytes("extra_info"),
                Bytes.toBytes("married"),
                Bytes.toBytes("false"));

        puts.add(put01);
        puts.add(put02);
        puts.add(put03);
        puts.add(put04);
        puts.add(put05);
        puts.add(put06);
        puts.add(put07);
        puts.add(put08);

        table.put(puts);
        table.close();
        conn.close();
    }

    /**
     * 5. 读取数据 ---get：一次读一行
     */
    public void getDate(String tableName) throws IOException {
        Table table = conn.getTable(TableName.valueOf(tableName));
        // 构造一个 get 查询参数对象，指定要 get 的是哪一行
        Get get = new Get("user002".getBytes());
        Result result = table.get(get);
        CellScanner cellScanner = result.cellScanner();
        while (cellScanner.advance()) {
            Cell current = cellScanner.current();
            byte[] familyArray = current.getFamilyArray();
            byte[] qualifierArray = current.getQualifierArray();
            byte[] valueArray = current.getValueArray();
            System.out.print(new String(familyArray,
                    current.getFamilyOffset(),
                    current.getFamilyLength()));
            System.out.print(":"
                    + new String(qualifierArray,
                    current.getQualifierOffset(),
                    current.getQualifierLength()));
            System.out.println(" "
                    + new String(valueArray, current.getValueOffset(),
                    current
                            .getValueLength()));
        }
        table.close();
        conn.close();
    }
    /**
     * 6. 删除表中的列数据
     */
    public void deleteData(String tableName) throws IOException {
        Table table = conn.getTable(TableName.valueOf(tableName));
        Delete delete = new Delete("user001".getBytes());
        delete.addColumn("base_info".getBytes(),"password".getBytes());
        table.delete(delete);
        table.close();
        conn.close();

    }
    public void printCell(Result result) throws IOException {
        CellScanner cellScanner = result.cellScanner();
        while (cellScanner.advance()) {
            Cell current = cellScanner.current();
            byte[] familyArray = current.getFamilyArray();
            byte[] qualifierArray = current.getQualifierArray();
            byte[] valueArray = current.getValueArray();
            System.out.print(new String(familyArray,
                    current.getFamilyOffset(),
                    current.getFamilyLength()));
            System.out.print(":"
                    + new String(qualifierArray,
                    current.getQualifierOffset(),
                    current.getQualifierLength()));
            System.out.println(" "
                    + new String(valueArray, current.getValueOffset(),
                    current
                            .getValueLength()));
        }
    }

    /**
     * 7.scan 批量查询数据
     */
    public void ScanData(String tableName) throws IOException {
      Table table = conn.getTable(TableName.valueOf(tableName));
        Scan scan=new Scan(Bytes.toBytes("liu_sh_01"),
                Bytes.toBytes("zhang_bj_01" + "\000"));
         ResultScanner scanner = table.getScanner(scan);
        Iterator<Result> iterator = scanner.iterator();
        while(iterator.hasNext()){
            Result result = iterator.next();
            printCell(result);
            System.out.println("-----------------------");
        }
    }

    /**
     * 8. 分页查询
     */
    public void pageScan(String tableName) throws IOException {
        final byte[] POSTFIX = new byte[] { 0x00 };
        Table table = conn.getTable(TableName.valueOf(tableName));
        Filter filter = new PageFilter(3); // 一次需要获取一页的条数
        byte[] lastRow = null;
        int totalRows = 0;
        while(true){
            Scan scan = new Scan();
            scan.setFilter(filter);
            if(lastRow!=null){
                byte[] startRow = Bytes.add(lastRow, POSTFIX); // 设置本次查询的起始行键
                scan.setStartRow(startRow);
            }
            ResultScanner scanner = table.getScanner(scan);
            int localRows = 0;
            Result result;
            while((result = scanner.next()) != null){
                System.out.println(++localRows + ":" + result);
                totalRows++;
                lastRow = result.getRow();
            }
            scanner.close();
            if(localRows == 0){
                break;
            }
        }
        System.out.println("total rows:" + totalRows);
    }

    public static void main(String[] args) throws IOException {
        HbaseDemo client = new HbaseDemo();
        String tableName = "emp_info";
        String colFamilys[] = {"base_info", "extra_info"};
        //建表
        //client.createHbaseTable(tableName,colFamilys);
        //修改schema
        //client.modifySchema(tableName);
        //插入数据
        //client.putData(tableName);
        //get
       // client.getDate(tableName);
        //scan
        //client.ScanData(tableName);
        //pageScan
        client.pageScan(tableName);
    }
}
