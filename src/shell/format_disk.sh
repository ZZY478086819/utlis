#!/usr/bin/env bash
# 这个脚本是用于批量挂盘使用的，当服务器的磁盘有多块需要挂载，用它比较方便
cp /etc/fstab /etc/fstab_`date "+%Y%m%d"`.bak

disk_list="/dev/sdb#/mnt/disk01 /dev/sdc#/mnt/disk02 /dev/sdd#/mnt/disk03"
for disk in $disk_list
do
    device=`echo ${disk}|awk -F '#' ' {print $1} '`
    mount_dir=`echo ${disk}|awk -F '#' ' {print $2} '`
    echo "+++++++++++++++++++++ create mount dir ${mount_dir} +++++++++++++++++++++ "
    mkdir -p ${mount_dir}

    echo "+++++++++++++++++++++ create partitiion for ${device} +++++++++++++++++++++ "
    parted -s ${device} mklabel gpt mkpart gpt2t ext2 0% 100%

    echo "+++++++++++++++++++++ formattiing ${device}  +++++++++++++++++++++"
    mkfs.ext4 -F ${device}

    echo "+++++++++++++++++++++ get ${device} UUID   +++++++++++++++++++++"
    UUID=`blkid "${device}" | awk ' {print $2} '|sed s/\"//g`

    echo "+++++++++++++++++++++ add  ${device} to /etc/fstab    +++++++++++++++++++++"
    echo "appending \"${UUID}${mount_dir} ext4 defaults 0 0 \" to /etc/fstab"
    echo "${UUID} ${mount_dir} ext4 defaults 0 0 ">> /etc/fstab
done
#mount all partitions
mount -a
#show mounted partitions
df -h