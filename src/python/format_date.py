import time

#日期字符串转换为时间戳
def str_to_timestamp(date_str,format="%Y-%m-%d %H:%M:%S"):
    timeStruct =time.strptime(date_str,format)  #将时间字符串转化为时间元组
    timeStamp =int(time.mktime(timeStruct))  #将当前时间转换为时间戳
    return timeStamp


#时间戳转化为时间字符串
def timestamp_to_str(ts,format="%Y-%m-%d %H:%M:%S"):
    timeStruct =time.localtime(ts) #将时间戳转换为时间元组
    strTime =time.strftime(format,timeStruct)  #将时间元组转化为时间字符串
    return strTime

#格式切换
def format_change(date_str,old_format="%Y-%m-%d %H:%M:%S",new_format="%Y-%m-%d %H:%M:%S"):
    timeStruct =time.strptime(date_str,old_format)
    new_date_str=time.strftime(new_format,timeStruct)
    return new_date_str

#当前时间的格式转换
def current_time_format(format="%Y-%m-%d %H:%M:%S"):
    timeStruct = time.localtime(time.time()) #将时间戳转换为时间元组
    date_format_str=time.strftime(format,timeStruct)
    return date_format_str

if __name__== "__main__":
    # 日期字符串转换为时间戳
    print(str_to_timestamp("2017-11-24 17:30:00"))
    # 时间戳转化为时间字符串
    print(timestamp_to_str(1511515800,"%Y-%m-%d %H:%M:%S"))
    # 格式切换
    print(format_change("2017-11-24 17:30:00",new_format="%Y/%m/%d %H:%M:%S"))
    # 当前时间的格式转换
    print(current_time_format("%Y/%m/%d %H:%M:%S"))

