#_*_coding:utf-8_*_

import random
#该方法用于生产一个任意位数随机验证码，仅包括数字和字母
def create_verification_code(num=4):
    checkcode=''
    for i in range(num):
        current = random.randrange(0, num)  # 生成随机数与循环次数比对
        current1=random.randrange(0, num)
        if current ==i:
            tmp = chr(random.randint(65, 90))  # 65~90为ASCii码表A~Z
        elif current1 == i:
            tmp = chr(random.randint(97, 122))  # 97~122为ASCii码表a~z
        else:
            tmp=random.randint(0,9)
        checkcode+=str(tmp)
    return checkcode

if __name__=="__main__":
    print(create_verification_code(4))
